# CanWork Landing page

This is the new CanWork landing page, a Static, Server-Rendered, Firebase Mini-app.
- Firebase Cloud Functions
- Expressjs
- Mustache
- Firestore

## Versions of used tools

- node v14.21.3 (old was v10.22.0)
- npm v6.14.18 (old was v6.14.7)
- firebase-tools v10.9.2 (old was v8.9.2)
- firebase-admin v8.10.0
- firebase-functions 3.6.1
- express v4.17.1
- mustache-express v1.3.0
- Java version 1.8 or higher (for Firestore local emulator) (sudo apt install default-jre on Debian recent versions)

## Compatibility

If you are on windows, you will have a hard/impossible time. Best to install + use WSL or run a linux VM

## Setting Up Development Environment

Install nvm to handle node.js versions (optional):

[Nvm installing](https://github.com/nvm-sh/nvm#installing-and-updating)

Install node.js:
```
nvm install 14.21.3
```

Install correct npm version:
```
npm install -g npm@6.14.18
```

Install Firebase CLI (new auth method):
```
npm install -g firebase-tools@10.9.2
```

Install Firebase CLI (old auth method):
```
npm install -g firebase-tools@9.23.0
```
These versions have been tested with issues in some cases:
```
npm install -g firebase-tools@8.9.2 (issues with timeouts on nvm, might be fine in other environments like flat linux/mac)
```
These versions have been tested are are not compatible with node 10:
```
npm install -g firebase-tools@10.6.0 (needs node > 12)
npm install -g firebase-tools@11.13.0 (needs node 14.18.0 || >=16.4.0)
```

## Credentials (json method, see below for login method)

- Open the Service Accounts pane of the Google Cloud Console.
- Make sure that App Engine default service account is selected, and use the options menu at right to select Create key
- When prompted, select JSON for the key type, and click Create
- Copy the generated JSON into google_credentials folder into your project home as key.json

## Install dependencies (root folder & then functions folder):
```
npm install 
## Or for yarn: yarn
cd functions
npm install 
## Or for yarn: yarn
cd ..
```

## Setup firebase credentials (login method)
- Login to firebase from remote vps:
```
firebase login --no-localhost
```

- Login to firebase (with gui):
```
firebase login
```
- Do the login prompts

- Then create the firebase config file:
```
### For linux/mac save in UTF-16:
firebase functions:config:get > .runtimeconfig.json
### Or for windows save in UTF-8:
firebase functions:config:get | ac .runtimeconfig.json
```
- Manually move the created `.runtimeconfig.json` file in the root folder to the `\functions` folder

## Run locally:
```
npm run emulator-standalone-stagingdb
```
- Open browser to 127.0.0.1:5000 for live preview

## Run local emulator:

Run local emulator:
```
npm run emulator
```


## Lint (check before deploy):

```
npm run lint

```


## Deploy:

Deploy:
```
export GOOGLE_APPLICATION_CREDENTIALS="google_credentials/key.json"
firebase login --no-localhost
cd functions
npm run deploy
```


## Initialize Cloud Functions (only for new project creation):
Set your Google default credentials to point to the key downloaded from service account pane:
```
export GOOGLE_APPLICATION_CREDENTIALS="google_credentials/key.json"
```

Init firebase functions:
```
firebase init functions
```
- Select an existing project
- Select staging-can-work
- Select Javascript language
- Select to use ESLint


