const express = require('express');
const router = express.Router();
const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const asyncHandler = require('express-async-handler');


router.get('/', asyncHandler(async (req, res, next) => {
  
  /* 2024-11 redirect all landing pages to main site, except for admin features */
  return res.redirect('https://canwork.io');
  /* ********** */
  
  
  const articlesCollection = db.collection('articles');
  let groups = [];
  const articlesSnapshot = await articlesCollection.where('category', "==", 'guides').orderBy('datePosted', 'desc').get();
  let index = 0;
  if (!articlesSnapshot.empty) {
    articlesSnapshot.forEach((doc) => {
        let obj = doc.data();
        obj.id = doc.id;
        obj._index = index;
        if (obj.category) obj.articleUrl = '/'+obj.category+"/"+obj.slug;
          else obj.articleUrl = "/"+obj.slug;
        if (index % 4 === 0) {
          // first of group
          obj._big = 'Y';
          let group = {articles:[]};
          group.articles.push(obj);
          groups.push(group);         
        } else {
          // row of 3
          obj._big = '';
          groups[groups.length-1].articles.push(obj);         
        }
        
        index++;
    });        
  }
  
  res.render('guides', {groups: groups}); 

}));

module.exports = router;