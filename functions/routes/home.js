const functions = require('firebase-functions');
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler')
const admin = require('firebase-admin');
const db = admin.firestore();
const articlesCollection = db.collection('articles');
const usersCollection = db.collection('users');
const avatarParse = require('../commons/util').avatarParse;

router.get('/', asyncHandler(async (req, res, next) => {

  /* 2024-11 redirect all landing pages to main site, except for admin features */
  return res.redirect('https://canwork.io');
  /* ********** */
  
  
  const categoriesArray = ["SOFTWARE DEVELOPERS", "DESIGNERS & CREATIVES", "MARKETING & SEO"];
  const chooseFromTopUsers = 4;
  
  
  function uniqueRandoms(iqty, min, max){
    var arr = [];
    var qty = iqty;
    if (qty > max - min + 1) qty = max - min + 1;
    while(arr.length < qty){
        var r = Math.floor(Math.random() * (max-min+1))+min;
        if(arr.indexOf(r) === -1) arr.push(r);
    } 
    return arr;
  }

  
  let articles = [];
  const articlesSnapshot = await articlesCollection.orderBy('datePosted', 'desc').limit(3).get();
  if (!articlesSnapshot.empty) {
    articlesSnapshot.forEach((doc) => {
        let obj = doc.data();
        obj.id = doc.id;
        if (obj.category) obj.articleUrl = '/'+obj.category+"/"+obj.slug;
          else obj.articleUrl = "/"+obj.slug;
        articles.push(obj);
    });        
  }

  let users = {};
  let usersChoosen = {};
  let usersExported = {};
  categoriesArray.forEach(category => {
    users[category] = [];
    usersExported[category] = [];
  });  
  const usersSnapshot = await usersCollection.where("category", "in", categoriesArray).get();

  if (!usersSnapshot.empty) {
    functions.logger.info(Date.now() + ' Retrieving users data: '+usersSnapshot.size);          
    usersSnapshot.forEach((doc) => {
      let obj = doc.data();
      
      let {avatarUrl, avatarInitials} = avatarParse(obj.avatar, obj.name);
      obj.avatarUrl = avatarUrl;
      obj.avatarInitials = avatarInitials;
      
      users[obj.category].push(obj);
    });



    categoriesArray.forEach(category => {
     functions.logger.info(Date.now() + ' sorting '+ category + ': '+users[category].length);          
     users[category].sort((a, b) => {
        // first consider if rating average is present
        if (!a.rating && b.rating) return 1;
        if (a.rating && !b.rating) return -1;
        // then consider rating average
        if (a.rating && b.rating) {
          if (a.rating.average > b.rating.average) return -1;
          if (a.rating.average < b.rating.average) return 1;
          // same average, consider rating count
          if (a.rating.count > b.rating.count) return -1;
          if (a.rating.count < b.rating.count) return 1;
          // same average and count, consider if verified
          if (!a.verified && b.verified) return 1;
          if (a.verified && !b.verified) return -1;
          // they are equal
          return 0;
        }
        return 0;
      });
      let usersLimit = chooseFromTopUsers - 1;
      if (users[category].length < usersLimit + 1) usersLimit = users[category].length - 1;
      usersChoosen[category] = uniqueRandoms(3, 0, usersLimit);
      usersChoosen[category].forEach(arIndex => {
        users[category][arIndex].ratingCalc = 0;
        users[category][arIndex].half = '';
        if ((users[category][arIndex]["rating"]) && (users[category][arIndex]["rating"]["average"])) {
          users[category][arIndex].ratingCalc = Math.floor(users[category][arIndex]["rating"]["average"]);
          if ((users[category][arIndex]["rating"]["average"] % 1) > 0) users[category][arIndex].half = " half";
        }
        usersExported[category].push(users[category][arIndex]);
      });
      return 0;
    });

  
  }
  
//Hack: temporary grab of 

  res.render('home', {
    articles: articles, 
    devs: usersExported[categoriesArray[0]],
    dess: usersExported[categoriesArray[1]],
    mars: usersExported[categoriesArray[2]]
  });
  
}));

module.exports = router;
