const express = require('express');
const router = express.Router({ mergeParams: true });
const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const asyncHandler = require('express-async-handler');
const avatarParse = require('../commons/util').avatarParse;
const isAdmin = require('../commons/util').isAdmin;
let md = require('markdown-it')();

router.get('/', asyncHandler(async (req, res, next) => {
  
  /* 2024-11 redirect all landing pages to main site, except for admin features */
  return res.redirect('https://canwork.io');
  /* ********** */
    

  let slug = req.params.slug;
  const usersCollection = db.collection('users');
  const querySnapshot = await usersCollection.where("slug", "==", slug).get();
  if (!querySnapshot.empty) {
    let user = querySnapshot.docs[0].data();
    let userId = querySnapshot.docs[0].id;
    user._id = userId;
    
    
    const reviewCollection = db.collection('reviews');
    const reviewsSnapshot = await reviewCollection.where("revieweeId", "==", userId).get();
    let reviews = [];
    if (!reviewsSnapshot.empty) {
      reviewsSnapshot.forEach(doc => {
        let review = doc.data();
        review.ratingCalc = 0;
        review.half = '';
        if (review.rating) {
          review.ratingCalc = Math.floor(review["rating"]);
          if ((review["rating"] % 1) > 0) review.half = " half";
        }
        review.timestamp = `${new Date(review.createdAt).toDateString()}`;
        reviews.push(review);
      });
    }
    user.reviews = reviews;

    const portfolioCollection = db.collection(`portfolio/${userId}/work`);
    const portfolioSnapshot = await portfolioCollection.orderBy('timestamp', 'desc').get();
    let portfolios = [];
    if (!portfolioSnapshot.empty) {
      portfolioSnapshot.forEach(doc => {
        let portfolio = doc.data();
        portfolio.image = portfolio.image || '/img/work-placeholder.svg';
        let skills = [];
        if (portfolio.tags) portfolio.tags.forEach(skill => {
          skills.push({skill, encoded: encodeURI(skill)});
        });
        portfolio.skills = skills;        
        portfolios.push(portfolio);        
      });
      
    }
    user.portfolios = portfolios;
    
    const certificationCollection = db.collection(`users/${userId}/certifications`);
    const certificationSnapshot = await certificationCollection.get();
    let certifications = [];
    if (!certificationSnapshot.empty) {
      certificationSnapshot.forEach(doc => {
        let certification = doc.data();
        certifications.push(certification);        
      });
    }
    user.certifications = certifications;
    
    

   

    let {avatarUrl, avatarInitials} = avatarParse(user.avatar, user.name);
    user.avatarUrl = avatarUrl;
    user.avatarInitials = avatarInitials;

    let skills = [];
    if (user.skillTags) user.skillTags.forEach(skill => {
      skills.push({skill, encoded: encodeURI(skill)});
    });
    user.skills = skills;

    
    user.descriptionMD = md.render(user.description || '');
    user.localTime = '';
    if (user.timezone && user.offset) {
      let localTime = new Date(new Date().toLocaleString('en-US', {
        timeZone: user.timezone || 'America/New_York',
      }));
      user.localTime = localTime.getHours().toString().padStart(2, "0") + ':'+localTime.getMinutes().toString().padStart(2, "0");
    }
    user.color0 = user.colors[0] || '#00FFCC';
    user.color1 = user.colors[1] || '#33ccff';
    
    user.ratingCalc = 0;
    user.half = '';
    if ((user["rating"]) && (user["rating"]["average"])) {
      user.ratingCalc = Math.floor(user["rating"]["average"]);
      if ((user["rating"]["average"] % 1) > 0) user.half = " half";
    }
    
    user.isProvider = false;
    if (user.type === 'Provider') user.isProvider = true;
    user.shareLink = `${res.locals.landingPath}/profile/${encodeURIComponent(slug)}`;
    
    
    user.OgTagsActive = true;
    user.OgTagsType = 'profile';
    user.OgTagsTitle = user.name;
    user.OgTagsDescription = user.title;
    user.OgTagsUrl = user.shareLink;
    user.OgTagsImage = avatarUrl;
    
    if (user.richAvatarUrl) {
      user.OgTagsImage = user.richAvatarUrl;
      user.OgTagsDescription = user.bio || user.title;      
    }
    
    user.chatUrl = `${res.locals.mainPath}auth/login?returnUrl=${encodeURIComponent('/profile/'+slug)}&nextAction=chat`;
    user.proposeUrl = `${res.locals.mainPath}auth/login?returnUrl=${encodeURIComponent('inbox/post/'+user.address)}`;
    
    user._isAdmin = isAdmin(req);
    if (user._isAdmin) {
      // let's check if this user is to whitelist and if so show the button
      // only if admin
      user._isToWhitelist = !user.whitelisted && !user.whitelistRejected && user.whitelistSubmitted;      
    }

    res.render('profile', user);
  } else {
    functions.logger.info('User slug not found: '+slug);      
    res.redirect('/');
  }
  


}));


module.exports = router;