const os = require('os');
const fs = require('fs');
const path = require('path');
const express = require('express');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const asyncHandler = require('express-async-handler')
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const Busboy = require('busboy');
const articleTemplate = require('../commons/article').articleTemplate;
const isAdmin = require('../commons/util').isAdmin;

/* 2fa imports */
const QRCode = require('qrcode')
const { getSettingsId, getSettings, isTfaEnabled, checkTfaCode, generateTfaSecret, generateTfaKeyUri } = require('../commons/tfa');

module.exports = function(app, passport, storage) {
  const articlesCollection = db.collection('articles');
  const usersCollection = db.collection('users');
  const bucket = storage.bucket();

  const settingsCollection = db.collection('settings');


  function loggedInMiddleware(req, res, next) {
      if (isAdmin(req))  {
        return next();
      } else {
        return res.redirect('/admin/login');
      }
  }  
  
  router.get('/login',
    asyncHandler(async (req, res, next) => {
      const tfaEnabled = await isTfaEnabled();
      
      res.render('admin/login', {tfa: tfaEnabled});
    })
  );

  
  router.post('/login', 
    passport.authenticate('local', { successRedirect: '/admin', failureRedirect: '/admin/login'})
  );
  
  router.get('/logout',
    loggedInMiddleware,
    (req, res) => {
      req.logout();
      res.redirect('/');
  });
  
  // check 2fa code and enable it, only logged in admin
  router.post('/2fa-setup',
    loggedInMiddleware,
    asyncHandler(async (req, res, next) => {
      const code = req.body.code;
      
      if (! (await checkTfaCode(code)) ) {
        // wrong code
        functions.logger.info('Wrong authenticator code provided into setup: '+code);
        res.redirect('/'); // go to home page
        return;
      }
      
      // finally enable it
      const settingsId = await getSettingsId();
      await settingsCollection.doc(settingsId).update({tfaEnabled: true});
      res.redirect('/admin');
      
      
  }));


  // enable or disable 2fa, only logged in admin
  router.get('/2fa',
    loggedInMiddleware,
    asyncHandler(async (req, res, next) => {
      const tfaEnabled = await isTfaEnabled();
      if (tfaEnabled) {
        // disable 2fa
        // remove secret from db and redirect to dashboard
        const settingsId = await getSettingsId();
        await settingsCollection.doc(settingsId).update({
          secret: '',
          tfaEnabled: false
        });
        res.redirect('/admin');
      } else {
        // enable 2fa
        // show qr code
        
        // generate secret
        const host = (req.headers['x-forwarded-host'] || req.headers.host || 'domain').split(":")[0];
        const secret = generateTfaSecret(); // base32 encoded hex secret key
        const email = 'admin@'+host; // symbolic admin email, customized with env
        QRCode.toDataURL(generateTfaKeyUri(email, 'Canwork Blog', secret), async (err, url) => {
          if (err) {
            functions.logger.error(err);            
            res.redirect('/admin');
            return;
          }
          // save secret, do not enable yet, until we check code
          const settingsId = await getSettingsId();
          await settingsCollection.doc(settingsId).update({secret});
          
          
          res.render('admin/2fa', {qr: url, email});

        })        
        
      }

  }));
  

  router.get('/',
    loggedInMiddleware,
    asyncHandler(async (req, res, next) => {
      // article list page
      let articles = [];
      const querySnapshot = await articlesCollection.orderBy('datePosted', 'desc').get();
      if (!querySnapshot.empty) {
        querySnapshot.forEach((doc) => {
            let obj = doc.data();
            obj.id = doc.id
            articles.push(obj);
        });        
      }
      
      const tfaEnabled = await isTfaEnabled();
      res.render('admin/dashboard', {articles: articles, enabled: tfaEnabled});
      
      
      //res.render('admin/dashboard', {articles: articles});
    }));
    
  router.get('/delete/:slug',
    loggedInMiddleware,
    asyncHandler(async (req, res, next) => {
      let slug = req.params.slug;
      // slug is mandatory
      if (slug) {
        const querySnapshot = await articlesCollection.where("slug", "==", slug).get();
        if (!querySnapshot.empty) {
          functions.logger.info('Deleting '+slug + ' on id ' +querySnapshot.docs[0].id);          
          await articlesCollection.doc(querySnapshot.docs[0].id).delete();
          
        } else {
          functions.logger.info('Not found Deleting slug '+slug);          
          
        }
        
      }

      res.redirect('/admin');
    

  }));    
    
  router.get('/edit/:slug',
    loggedInMiddleware,
    asyncHandler(async (req, res, next) => {
      // article edit
        let slug = req.params.slug;

  

        const querySnapshot = await articlesCollection.where("slug", "==", slug).get();
        // safe empy default
        let article = {}
        Object.assign(article, articleTemplate);
        
        
        if (!querySnapshot.empty) {
          Object.assign(article, querySnapshot.docs[0].data());
          
          // add safe default
          if (!article.imageBodyArray || (article.imageBodyArray.length === 0)) article.imageBodyArray = '[]';
          
          article.id = querySnapshot.docs[0].id;
          article.tagsFlat = '';
          article.tags.forEach(el => {
            if (article.tagsFlat) article.tagsFlat += ', ';
            article.tagsFlat += el.trim();
          })
          article.escapedimageBodyArray = article.imageBodyArray.replace(/"/g, '&quot;');      
          res.render('admin/edit', article);
            
        } else {
          functions.logger.info('Slug not found: '+slug);      
          res.redirect('/admin');
        }
    }));
    
  router.get('/edit',
    loggedInMiddleware,
    asyncHandler(async (req, res, next) => {
      // new article
      let article = {}
      Object.assign(article, articleTemplate);

      
      
      res.render('admin/edit', article);
  }));    
  
  router.post('/upload',
      loggedInMiddleware,
      asyncHandler(async (req, res, next) => {

        const uuid = uuidv4().replace(/-/g, "");
        let imageTobeUploaded;
        let imageFileName, imageUrl;
        let busboy = new Busboy({headers: req.headers});
      
        busboy.on('file', (fieldname, file, filename, encoding, mimeType) => {
          const imageExtension = filename.split('.')[filename.split('.').length - 1];
          imageFileName = `${uuid}.${imageExtension}`;
          const filepath = path.join(os.tmpdir(), imageFileName);          
          imageTobeUploaded = {filepath, mimeType};
          
          file.pipe(fs.createWriteStream(filepath)); 

          
        });
        
        busboy.on('finish', () => {
            let destination = 'uploads/articles/'+imageFileName;
            bucket.upload(imageTobeUploaded.filepath, {
                destination: destination,
                resumable: false,
                metadata:{
                    metadata:{
                        contentType:imageTobeUploaded.mimeType
                    }
                }
            })
            .then( () => {
                imageUrl = `https://firebasestorage.googleapis.com/v0/b/${JSON.parse(process.env.FIREBASE_CONFIG).storageBucket}/o/${encodeURIComponent(destination)}?alt=media`
                functions.logger.info(imageUrl);
                return {}
            })
            .then( async () => {
                // return imageUrl to form for later saving, only on save button

                return res.json({message: "Image Uploaded Successfully",imageUrl:imageUrl});
            })
            .catch(err => {
                functions.logger.error(err);
                return res.status(400).json({ error : err.code});
            })
        });        
        
        
        //file.pipe(fstream); 
        if (req.rawBody) {
          busboy.end(req.rawBody);
        } else {
          req.pipe(busboy);
        }      
  })); 
  
  // delete an user
  router.post('/rmu',
   loggedInMiddleware,
   asyncHandler(async (req, res, next) => {
     // uid is mandatory
     if (!req.body.u) return res.status(400).json({ error : 'no param'});
     if (req.body.u.length < 20) return res.status(400).json({ error : 'invalid param'});
     await usersCollection.doc(req.body.u).delete();
     functions.logger.info('Deleted user id '+req.body.u);
       
     return res.status(200).json({ status : 'ok'});
  }));

  // whitelist an user
  router.post('/wmu',
   loggedInMiddleware,
   asyncHandler(async (req, res, next) => {
     // uid is mandatory
     if (!req.body.u) return res.status(400).json({ error : 'no param'});
     if (req.body.u.length < 20) return res.status(400).json({ error : 'invalid param'});
     await usersCollection.doc(req.body.u).update({
       whitelisted: true
     });
     functions.logger.info('Whitelisted user id '+req.body.u);
       
     return res.status(200).json({ status : 'ok'});
  }));     

  router.post('/edit',
    loggedInMiddleware,
    asyncHandler(async (req, res, next) => {
      // slug is mandatory
      if (req.body.slug) {
        
        if (req.body.id) {
          // edit
          // check slug for duplicates
          const querySnapshot = await articlesCollection.where("slug", "==", req.body.slug).get();
          if (!querySnapshot.empty) {
            if (querySnapshot.docs[0].id === req.body.id) {
              // editing existing one, let's go on
              functions.logger.info('Saving '+req.body.slug + ' on id ' +querySnapshot.docs[0].id);
              let article = {};
              Object.assign(article, req.body);
              article.tags = [];
              if (article.tagsFlat.trim()) article.tags = article.tagsFlat.trim().split(',');
              delete article.tagsFlat;
              delete article.id;
              article.imageBodyArray = article.imageBodyArray.replace(/&quot;/g, '"');
              // datePosted cannot be null, otherwise we can't sort articles
              if (!article.datePosted) article.datePosted = (new Date()).toISOString().substring(0,10);
              await articlesCollection.doc(querySnapshot.docs[0].id).update(article);
                          
              
            } else {
              functions.logger.info('Forbidden editing duplicate slug '+req.body.slug + ' on id ' +querySnapshot.docs[0].id);      
            }
          }
          
        } else {
          // new
          // check slug for duplicates
          const querySnapshot = await articlesCollection.where("slug", "==", req.body.slug).get();
          if (!querySnapshot.empty) {
            functions.logger.info('Forbidden adding duplicate slug '+req.body.slug + ' on id ' +querySnapshot.docs[0].id);            
          } else {
            // adding new one, let's go on
            functions.logger.info('Adding '+req.body.slug + ' on new id');
            let article = {};
            Object.assign(article, req.body);
            article.tags = [];
            if (article.tagsFlat.trim()) article.tags = article.tagsFlat.trim().split(',');
            delete article.tagsFlat;
            delete article.id;
            article.imageBodyArray = article.imageBodyArray.replace(/&quot;/g, '"');
            
            // datePosted cannot be null, otherwise we can't sort articles
            if (!article.datePosted) article.datePosted = (new Date()).toISOString().substring(0,10);
            await articlesCollection.add(article);          
          }
          
          
        }
      }
 
      res.redirect('/admin');
  }));   
  
  return router;
}
