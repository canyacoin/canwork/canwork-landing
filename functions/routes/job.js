const express = require('express');
const router = express.Router({ mergeParams: true });
const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const asyncHandler = require('express-async-handler');
const util = require('../commons/util');


router.get('/', asyncHandler(async (req, res, next) => {

  /* 2024-11 redirect all landing pages to main site, except for admin features */
  return res.redirect('https://canwork.io');
  /* ********** */
  

  let slug = req.params.slug;
  const jobsCollection = db.collection('public-jobs');
  const usersCollection = db.collection('users');
  
  const querySnapshot = await jobsCollection.where("slug", "==", slug).get();
  if (!querySnapshot.empty) {
    let job = querySnapshot.docs[0].data();
    let jobId = querySnapshot.docs[0].id;
    
    if (job.visibility !== 'public') {
      functions.logger.info('Job not public: '+slug);      
      res.redirect('/');
      return;
    }
    
    if (job.draft) {
      functions.logger.info('Job draft: '+slug);      
      res.redirect('/');
      return;
    }

    
    let ogDescription = job.information.description || ''; // before linkify
    ogDescription = ogDescription.replace(/<[^>]*>?/gm, '');
    if (ogDescription.length > 82) ogDescription = ogDescription.substring(0, 80)+'...';    
    
    if (job.information && job.information.description) job.information.description = util.linkify(job.information.description);
    
    job.shareLink = `${res.locals.landingPath}/jobs/public/${encodeURIComponent(slug)}`;
    
    job.attachmentName = '';
    if (job.information && job.information.attachments && (job.information.attachments.length > 0)) {
      job.attachmentName = job.information.attachments[0].name;
      job.attachmentUrl = `https://firebasestorage.googleapis.com/v0/b/${JSON.parse(process.env.FIREBASE_CONFIG).storageBucket}/o/${encodeURIComponent(job.information.attachments[0].filePath)}?alt=media`;
    }
    if (job.attachmentName.length > 10) job.attachmentName = job.attachmentName.substring(0, 10)+'...';
    let skills = [];
    if (job.information.skills) job.information.skills.forEach(skill => {
      skills.push({skill, encoded: encodeURI(skill)});
    });
    job.skills = skills;
    job.isOpen = (job.state === 'Accepting Offers');
    
    let user = {};
    
    const userRef = usersCollection.doc(job.clientId);
    const userSnapshot = await userRef.get();
    if (!userSnapshot.exists) {
      functions.logger.info('job clientId not found: '+job.clientId);      
    } else {
      user = userSnapshot.data();
      let {avatarUrl, avatarInitials} = util.avatarParse(user.avatar, user.name);
      user.avatarUrl = avatarUrl;
      user.avatarInitials = avatarInitials;      
    }
    job.user = user;
    
    job.paymentType = job.paymentType.toLowerCase();
    
    const bidCollection = db.collection(`public-jobs/${jobId}/bids`);
    const bidSnapshot = await bidCollection.get();
    let bids = [];
    if (!bidSnapshot.empty) {
      bidSnapshot.forEach(doc => {      
        bids.push(doc.id);  // we don't need the data, only count     
      });
      
    }    
    job.bids = bids;
    
    job.bidUrl = `${res.locals.mainPath}auth/login?returnUrl=${encodeURIComponent('/jobs/public/'+slug)}&nextAction=bid`;
    

    
    job.OgTagsActive = true;
    job.OgTagsType = 'article';
    job.OgTagsTitle = job.information.title;
    job.OgTagsDescription = ogDescription;
    job.OgTagsUrl = job.shareLink;
    job.OgTagsImage = "https://www.canwork.io/img/CanYa_OpenGraph_CanYa.png"; // same as default one

    if (job.richAvatarUrl) {
      job.OgTagsImage = job.richAvatarUrl;
    }
    
    res.render('job', job);
  } else {
    functions.logger.info('Job slug not found: '+slug);      
    res.redirect('/');
  }
  


}));


module.exports = router;    