const express = require('express');
const router = express.Router();
const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const asyncHandler = require('express-async-handler');
const util = require('../commons/util');


const providerTypes = [
  {
    name: 'All Jobs',
    img: 'writer.svg',
    id: 'all',
  },
  {
    name: 'Content Creators',
    img: 'writer.svg',
    id: 'contentCreator',
  },
  {
    name: 'Software Developers',
    img: 'dev.svg',
    id: 'softwareDev',
  },
  {
    name: 'Designers & Creatives',
    img: 'creatives.svg',
    id: 'designer',
  },
  {
    name: 'Financial Experts',
    img: 'finance.svg',
    id: 'finance',
  },
  {
    name: 'Marketing & Seo',
    img: 'marketing.svg',
    id: 'marketing',
  },
  {
    name: 'Virtual Assistants',
    img: 'assistant.svg',
    id: 'virtualAssistant',
  },
];

function getProviderTypeImage(string) {
  let url = '/img/providers/'
  const type = providerTypes.find(prov => prov.id === string)
  url = url + type.img
  return url
}

router.get('/', asyncHandler(async (req, res, next) => {
  
  /* 2024-11 redirect all landing pages to main site, except for admin features */
  return res.redirect('https://canwork.io');
  /* ********** */
  

  
  const jobsCollection = db.collection('public-jobs');
  const statsCollection = db.collection('statistics');
  let category = req.query.category || 'all';
  let searchTerm = req.query.search || '';
  let providers = [];
  providerTypes.forEach((provider) => {
    provider.selected = (provider.id === category);
    providers.push(provider);
  });
  
  let jobs = [];
  const jobsSnapshot = await jobsCollection.where('visibility', '==', 'public')
                                           .where('state', '==', 'Accepting Offers')
                                           .where('deadline', '>', new Date().toISOString().slice(0, 10))
                                           .get();
  if (!jobsSnapshot.empty) {
    jobsSnapshot.forEach((doc) => {
      let obj = doc.data();
      if ((category === 'all') || (category === obj.information.providerType)) {
      // filter category
       
        if ((searchTerm === '') || (JSON.stringify(obj).toLowerCase().includes(searchTerm.toLowerCase()))) {
        // filter searchTerm

          obj.providerTypeImage = getProviderTypeImage(obj.information.providerType);
          obj.jobUrl = `/jobs/public/${encodeURIComponent(obj.slug)}`;
          
          let jobDescription = obj.information.description || ''; // before linkify
          if (jobDescription.length >= 200) jobDescription = jobDescription.substring(0, 196)+' ...';    
          obj.jobDescription = util.linkify(jobDescription);
          obj.timestamp = util.timestampToDate(obj.actionLog[0].timestamp);
          let skills = [];
          if (obj.information.skills) obj.information.skills.forEach(skill => {
            skills.push({skill, encoded: encodeURI(skill)});
          });
          obj.skills = skills;

          jobs.push(obj);
        }
      }
    })
   jobs.sort((a, b) => (parseInt(b.actionLog[0].timestamp) - parseInt(a.actionLog[0].timestamp)));
     
  }
  
  //get job stats from statistic table
  let stats = {total:{count: '',usd: ''}}
  let jobStats = {count: 0,usd: 0}
  let publicJobStats = {count: 0,usd: 0}  
  
  const statsSnapshot = await statsCollection.get();  
  if (!statsSnapshot.empty) {
    statsSnapshot.forEach((doc) => {
      let obj = doc.data();
      if (obj.name === 'publicJobs') publicJobStats = obj;
      if (obj.name === 'jobs') jobStats = obj;

    });
  }

  stats.total = {
    count: jobStats.count + publicJobStats.count,
    usd: Math.round(jobStats.usd + publicJobStats.usd)
  }



  res.render('jobs', {jobs, providers, category, searchTerm, stats}); 

}));

module.exports = router;