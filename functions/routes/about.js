const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
  /* 2024-08 redirect all landing pages to blog, except for admin features */
  return res.redirect('/blog');
  /* ********** */
  
  res.send("About page");
});

module.exports = router;