const express = require('express');
const router = express.Router();
const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const asyncHandler = require('express-async-handler');
const util = require('../commons/util');
const env = functions.config();
const algoliaSearch = require('algoliasearch/lite');

const algoliaClient = algoliaSearch(env.algolia.appid, env.algolia.apikey);
const algoliaIndex = algoliaClient.initIndex(env.algolia.providerindex);

const catMap = {
  'contentCreator': 'Content Creators',
  'softwareDev': 'Software Developers',
  'designer': 'Designers & Creatives',
  'finance': 'Financial Experts',
  'marketing': 'Marketing & Seo',
  'virtualAssistant': 'Virtual Assistants',
}


router.get('/', asyncHandler(async (req, res, next) => {

  /* 2024-11 redirect all landing pages to main site, except for admin features */
  return res.redirect('https://canwork.io');
  /* ********** */  

  let query = req.query.query || '';
  let page = req.query.page || 0;

  let reqCategories = req.query.categories || '';
  let categories = [];
  let reqCategoriesArray = [];
  if (reqCategories) {
    reqCategoriesArray = reqCategories.split(',');
    reqCategoriesArray.forEach((cat) => {
      categories.push(`category:${catMap[cat]}`);
    });
  }
 
  let hourlyMin = 0;
  let hourlyMax = 300;
  
  let reqRange = req.query.range || '';
  let range = [];
  let reqRangeArray = [];
  if (reqRange) {
    reqRangeArray = reqRange.toString().split(',');
    if (reqRangeArray.length === 2) {
      hourlyMin = reqRangeArray[0];
      hourlyMax = reqRangeArray[1];
    }

  }  



   
  let hits;
  try {
    let searchParams = {
      page,
      facets: ["category", "hourlyRate"],
      maxValuesPerFacet: 10,   
      hitsPerPage: 15 // default is 20, but with 15 we have nice align one rows of three, and more pages
    }
    if (categories.length > 0) searchParams.facetFilters = [categories];
    
    hits = await algoliaIndex.search(query, searchParams);

  } catch (err) {
    functions.logger.info(err.message);
  }
  let providers = [];
  if (hits && hits.hits) hits.hits.forEach((obj) => {
    let {avatarUrl, avatarInitials} = util.avatarParse(obj.avatar, obj.name);
    obj.avatarUrl = avatarUrl;
    obj.avatarInitials = avatarInitials;
    obj.ratingCalc = 0;
    obj.half = '';
    if ((obj.rating) && (obj.rating.average)) {
      obj.ratingCalc = Math.floor(obj.rating.average);
      if (obj.rating.average % 1 > 0) obj.half = " half";
    }    
    
    providers.push(obj);
  })

  let queryResults;

   if (hits) {
     queryResults = {
      numPages: hits.nbPages,
      page: hits.page,
      first: false,
      last: false,
      pages: [],
      previous: hits.page - 1,
      next: hits.page + 1,
      url: `/search?`
    } 
   } else {
     queryResults = {
      numPages: 0,
      page: 0,
      first: false,
      last: false,
      pages: [],
      previous: -1,
      next: 1,
      url: `/search?`
     }
   }
  if (query) queryResults.url += `query=${query}&`;  
  if (reqCategories) queryResults.url += `categories=${reqCategories}&`;
  if (reqRange) queryResults.url += `range=${reqRange}&`;
  queryResults.url += 'page=';
  
  if (queryResults.numPages > 3) queryResults.numPages = 3; // don't let them see all the db
  let start = queryResults.page-1;
  let end = queryResults.page+1;
  if (queryResults.page === 0) {
    end = queryResults.page+2;
    queryResults.first = true;
  }
  if (queryResults.page === queryResults.numPages-1) {
    start = queryResults.page-2;
    queryResults.last = true;    
  }
  for (var i=start; i<=end;i++) {
    if ((i>=0) && (i<queryResults.numPages)) {
      queryResults.pages.push({
        pageName: i+1,
        pageNumber: i,
        current: (i===queryResults.page),
      });
    }
  }
   

  res.render('search', {query, providers, queryResults, reqCategoriesArray: JSON.stringify(reqCategoriesArray), reqRangeArray: JSON.stringify(reqRangeArray)}); 

}));

module.exports = router;  
