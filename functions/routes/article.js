const express = require('express');
const router = express.Router();
const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const asyncHandler = require('express-async-handler');
const articleTemplate = require('../commons/article').articleTemplate;


router.get('/', asyncHandler(async (req, res, next) => {

  /* 2024-11 redirect all landing pages to main site, except for admin features */
  return res.redirect('https://canwork.io');
  /* ********** */
  
  let slug = req.baseUrl.substr(req.baseUrl.lastIndexOf("/")+1);
  let previous = req.baseUrl.substr(0, req.baseUrl.lastIndexOf("/"));
  let categoryTentative = previous.substr(previous.lastIndexOf("/")+1);
  let category = '';
  if ((Boolean(categoryTentative)) && (categoryTentative !== 'landingSite')) category = categoryTentative;

  const articlesCollection = db.collection('articles');

  const querySnapshot = await articlesCollection.where("slug", "==", slug).get();
  
  // safe empty default
  let article = {}
  Object.assign(article, articleTemplate);
  
  if (!querySnapshot.empty) {
    let data = querySnapshot.docs[0].data();
    // check category
    if ((Boolean(data.category)) && (category !== data.category)) {
      functions.logger.info("category/slug not correct: '"+category+"/"+slug+"'");      
      res.redirect('/');
      return;
    }
    Object.assign(article, data);
    // article images
    let articleImages = article.body.match(/\[\[image\d+\]\]/gi);
    let imageArray;
    if (!article.imageBodyArray) imageArray = [];
      else imageArray = JSON.parse(article.imageBodyArray);
    if (articleImages) articleImages.forEach((img) => {
      let index=img.match(/\d+/)[0];
      if (index < (imageArray.length + 1))
        article.body = article.body.replace(img, '<img src="'+imageArray[index-1]+'" style="width:100%">');
      else 
        article.body = article.body.replace(img, '');
    });
    article.OgTagsActive = true;
    article.OgTagsType = 'article';
    article.OgTagsTitle = article.title || 'Blog';
    article.OgTagsDescription = article.subTitle || 'CanWork Blog Post';
    article.OgTagsUrl = res.locals.landingPath + req.originalUrl;
    article.OgTagsImage = article.imageUrl || "";
    
    
    res.render('article', article);
      
  } else {
    functions.logger.info('Article slug not found: '+slug);      
    res.redirect('/');
  }
      


}));

module.exports = router;
