const functions = require('firebase-functions');
const path = require('path');
const express = require('express');
const mustache = require('mustache-express');

const adminUser = require('./commons/util').adminUser;


// firestore
const firebaseAdmin = require('firebase-admin');
const firebaseRef = firebaseAdmin.initializeApp();
const firebaseDb = firebaseAdmin.firestore();
const settingsCollection = firebaseDb.collection('settings');


// storage
const storage = firebaseAdmin.storage();


/* tfa imports */
const { isTfaEnabled, checkTfaCode } = require('./commons/tfa');


// express
const app = express();
app.engine('mustache', mustache());
app.set('view engine', 'mustache');
app.set('views', path.join(__dirname,'/views'));


const bodyParser = require('body-parser');

const expressSession = require('express-session');
const FirebaseStore = require('connect-session-firebase')(expressSession);

//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSession({
  store: new FirebaseStore({
    database: firebaseRef.database()
  }),
  name: '__session',  
  secret: functions.config().fbase.session,
  resave: true,
  saveUninitialized: true
}));

//  passport

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;


passport.use(new LocalStrategy(
  {
    passReqToCallback: true // inject all request data so we can get tfa code if present
  },
  ((req, username, password, done) => {

    settingsCollection.where("name", "==", "adminPassword").get().then(async (querySnapshot) => {
      let docs = querySnapshot.docs;
      let correctPwd = docs[0].data().value;

      let codeCheck = true; // no tfa default
      const tfaEnabled = await isTfaEnabled();
      if (tfaEnabled) {
        const code = req.body.code;
        codeCheck = await checkTfaCode(code);
      }
      
      if ( codeCheck && (username === 'admin') && (password === correctPwd) ) return done(null, adminUser);
        else return done(null, false, { message: 'Incorrect username/password.' });      
    })
    .catch(err => { functions.logger.error(err) });    

  })
));

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

app.use(passport.initialize());
app.use(passport.session());

// locals
app.use((req, res, next) => {
  res.locals.mainPath = "https://app.canwork.io/"; // with final slash
  res.locals.landingPath = "https://canwork.io"; // no final slash
  next();
});



// routes
const home = require('./routes/home');
const admin = require('./routes/admin')(app, passport, storage);
const about = require('./routes/about');
const blog = require('./routes/blog');
const guides = require('./routes/guides');
const search = require('./routes/search');
const jobs = require('./routes/jobs');
const profile = require('./routes/profile');
const article = require('./routes/article');
const job = require('./routes/job');


app.use('/', home);
app.use('/admin', admin);
app.use('/about', about);
app.use('/blog', blog);
app.use('/guides', guides);
app.use('/search', search);
app.use('/jobs', jobs);
app.use('/jobs/public/:slug', job);
app.use('/profile/:slug', profile);
app.use('/:slug', article);
app.use('/:category/:slug', article); // optional category



// export firebase cloud function
exports.landingSite = functions.https.onRequest(app);
