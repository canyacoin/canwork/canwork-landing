const articleTemplate = {
    id: '',    
    slug: '',
    title: '',
    subTitle: '',
    body: '',
    tags: [],
    author: '',
    datePosted: '',
    category: '',
    imageUrl: '',
    imageBodyArray: JSON.stringify([])
}

exports.articleTemplate = articleTemplate;