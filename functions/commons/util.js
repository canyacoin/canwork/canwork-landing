function avatarParse(avatar, name) {
  let avatarUrl = '';
  let avatarInitials = '?';
  if (avatar && avatar.uri && avatar.uri.trim()) avatarUrl = avatar.uri.trim();
  if (name) avatarInitials = name.split(' ').map(str => str[0]).join('').substring(0, 2).toUpperCase();
  
  return {avatarUrl, avatarInitials}  
}

function linkify(text, userMessage) {
  const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi
  if (!userMessage) {
    text = text.replace(urlRegex, '<a target="_blank" href="$1">$1</a>')
  } else {
    text = text.replace(
      urlRegex,
      '<a class="text-white" target="_blank" href="$1">$1</a>'
    )
  }
  return text
}

function timestampToDate(timestamp) {
  const date = new Date(parseInt(timestamp, 10)).toLocaleDateString()
  return date
}

const adminUser = {id: 1};

function isAdmin(req) {
  
  if (req.user && (req.user.id === adminUser.id)) return true;
  
  return false;

}

exports.avatarParse = avatarParse;
exports.linkify = linkify;
exports.timestampToDate = timestampToDate;
exports.isAdmin = isAdmin;
exports.adminUser = adminUser;