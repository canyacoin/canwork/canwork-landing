const admin = require('firebase-admin');
const db = admin.firestore();
const { authenticator } = require('otplib')


const settingsCollection = db.collection('settings');

async function getSettingsId() {
  
  return new Promise(((resolve, reject) => {
    settingsCollection.where("name", "==", "adminPassword").get().then(querySnapshot => {
      let docs = querySnapshot.docs;
      let docId = docs[0].id;
      return resolve(docId);
    }).catch(err => { 
      functions.logger.error(err);
      resolve(null); 
    });        
  }));        
}

async function getSettings() {
  return new Promise(((resolve, reject) => {
    settingsCollection.where("name", "==", "adminPassword").get().then(querySnapshot => {
      let docs = querySnapshot.docs;
      let doc = docs[0].data();
      return resolve(doc);
    }).catch(err => { 
      functions.logger.error(err);
      resolve(null); 
    });        
  }));        
}

async function isTfaEnabled() {
  return new Promise(((resolve, reject) => {
    settingsCollection.where("name", "==", "adminPassword").get().then(querySnapshot => {
      let docs = querySnapshot.docs;
      let doc = docs[0].data();
      if ( 
         doc.hasOwnProperty("secret") && (doc.secret.length > 5) &&
         doc.hasOwnProperty("tfaEnabled") && doc.tfaEnabled === true
         ) return resolve(true);
      resolve(false);
      return 0; // lint
    }).catch(err => { 
      functions.logger.error(err);
      resolve(false); 
    });        
  }));        
}

async function checkTfaCode(code) {
  const settings = await getSettings();
  return authenticator.check(code, settings.secret);
}

function generateTfaSecret() {
  return authenticator.generateSecret() ;
}

function generateTfaKeyUri(email, app, secret) {
  return authenticator.keyuri(email, app, secret)
}

module.exports = { 
  getSettingsId,
  getSettings,
  isTfaEnabled,
  checkTfaCode,
  generateTfaSecret,
  generateTfaKeyUri
}
