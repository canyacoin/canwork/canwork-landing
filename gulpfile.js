const gulp = require("gulp");
const cleanCSS = require("gulp-clean-css");
const minifyJS = require('gulp-minify');
const rename = require('gulp-rename');


function minifyCss() {
  return (
    gulp
      .src("./src/css/*.css")
      .pipe(cleanCSS())
      .pipe(rename({
          suffix: '.min'
      }))      
      .pipe(gulp.dest("./public/css"))
  );
}

function minifyJs() {
  return (
    gulp
      .src("./src/js/*.js")
      .pipe(minifyJS({noSource: true}))    
      .pipe(gulp.dest("./public/js"))
  );
}

function watchCss() {
  return (
    gulp
      .src("./src/css/*.css")
      .pipe(rename({
          suffix: '.min'
      }))      
      .pipe(gulp.dest("./public/css"))
  );  
  
}

function watchJs() {
  return (
    gulp
      .src("./src/js/*.js")
      .pipe(rename(function(path) {
        path.basename += '-min';
      }))      
      .pipe(gulp.dest("./public/js"))
  );  
  
}



gulp.task("minify-css", minifyCss);
gulp.task("minify-js", minifyJs);
gulp.task("minify", gulp.parallel('minify-css', 'minify-js'));


gulp.task("watch-css", () => { gulp.watch("./src/css/*.css", watchCss); });
gulp.task("watch-js", () => { gulp.watch("./src/js/*.js", watchJs); });
gulp.task("watch", gulp.series(gulp.parallel(watchCss, watchJs), gulp.parallel('watch-css', 'watch-js')));

gulp.task("watch-css-minified", () => { gulp.watch("./src/css/*.css", minifyCss); });
gulp.task("watch-js-minified", () => { gulp.watch("./src/js/*.js", minifyJs); });
gulp.task("watch-minified", gulp.series(gulp.parallel(minifyCss, minifyJs), gulp.parallel('watch-css-minified', 'watch-js-minified')));
